package com.jsh.erp.datasource.mappers;

import com.jsh.erp.datasource.entities.DepotHead;
import com.jsh.erp.datasource.entities.DepotHeadExample;

import java.math.BigDecimal;
import java.util.List;

import com.jsh.erp.datasource.vo.DepotHeadVo4InDetail;
import com.jsh.erp.datasource.vo.DepotHeadVo4InOutMCount;
import com.jsh.erp.datasource.vo.DepotHeadVo4List;
import com.jsh.erp.datasource.vo.DepotHeadVo4StatementAccount;
import org.apache.ibatis.annotations.Param;

public interface DepotHeadMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int countByExample(DepotHeadExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int deleteByExample(DepotHeadExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int insert(DepotHead record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int insertSelective(DepotHead record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    List<DepotHead> selectByExample(DepotHeadExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    DepotHead selectByPrimaryKey(Long id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int updateByExampleSelective(@Param("record") DepotHead record, @Param("example") DepotHeadExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int updateByExample(@Param("record") DepotHead record, @Param("example") DepotHeadExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(DepotHead record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table jsh_depothead
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(DepotHead record);
}